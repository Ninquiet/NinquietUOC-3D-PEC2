using UnityEngine;

public class ParallaxEffect : MonoBehaviour
{
    [System.Serializable]
    public class ParallaxLayer
    {
        public Transform layerTransform;
        public float speedMultiplier;
    }

    public ParallaxLayer[] layers;
    public Camera mainCamera;

    private Vector3 previousCameraPosition;
    private Transform[] layerClones;

    void Start()
    {
        if (mainCamera == null)
        {
            mainCamera = Camera.main;
        }
        previousCameraPosition = mainCamera.transform.position;
        
        layerClones = new Transform[layers.Length];
        for (int i = 0; i < layers.Length; i++)
        {
            CreateLayerClone(i);
        }
    }

    void Update()
    {
        Vector3 deltaMovement = mainCamera.transform.position - previousCameraPosition;

        for (int i = 0; i < layers.Length; i++)
        {
            float parallax = deltaMovement.x * layers[i].speedMultiplier;
            layers[i].layerTransform.position += new Vector3(parallax, 0, 0);
            layerClones[i].position += new Vector3(parallax, 0, 0);

            CheckLayerPosition(i);
        }

        previousCameraPosition = mainCamera.transform.position;
    }

    void CreateLayerClone(int index)
    {
        Transform original = layers[index].layerTransform;
        Transform clone = Instantiate(original, original.position, original.rotation);
        clone.SetParent(original.parent);
        layerClones[index] = clone;

        float layerWidth = original.GetComponent<SpriteRenderer>().bounds.size.x;
        clone.position = original.position + new Vector3(layerWidth, 0, 0);
    }

    void CheckLayerPosition(int index)
    {
        Transform original = layers[index].layerTransform;
        Transform clone = layerClones[index];
        float layerWidth = original.GetComponent<SpriteRenderer>().bounds.size.x;

        if (mainCamera.transform.position.x - original.position.x > layerWidth)
        {
            original.position += new Vector3(layerWidth * 2, 0, 0);
        }
        else if (mainCamera.transform.position.x - clone.position.x > layerWidth)
        {
            clone.position += new Vector3(layerWidth * 2, 0, 0);
        }

        if (original.position.x - mainCamera.transform.position.x > layerWidth)
        {
            original.position -= new Vector3(layerWidth * 2, 0, 0);
        }
        else if (clone.position.x - mainCamera.transform.position.x > layerWidth)
        {
            clone.position -= new Vector3(layerWidth * 2, 0, 0);
        }
    }
}
