using System.Collections.Generic;

namespace Save
{
    [System.Serializable]
    public class SaveData
    {
        public float playerPositionX;
        public float playerPositionY;
    }
    
    [System.Serializable]
    public class SaveSlot
    {
        public List<SaveData> SaveDataList = new List<SaveData>();
    }
}