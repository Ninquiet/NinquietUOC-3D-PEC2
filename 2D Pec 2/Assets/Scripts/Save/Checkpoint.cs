using System;
using DG.Tweening;
using Sound;
using UnityEngine;

namespace Save
{
    public class Checkpoint : MonoBehaviour
    {
        [SerializeField] private Transform _tablero;
        
        private bool _isCheckpointActive = true;
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Player") || !_isCheckpointActive)
                return;
            
            var positionToSave = other.transform.position;
            positionToSave.y += 1;
            
            SaveSystem.SaveCheckpoint(positionToSave);
            _tablero.DORotate(new Vector3(0, 0, 0), 0.6f);
            GameSoundHandler.Instance.PlayAudio(Sound.AudioType.PlayerCheckpoint);
            _isCheckpointActive = false;
        }
    }
}