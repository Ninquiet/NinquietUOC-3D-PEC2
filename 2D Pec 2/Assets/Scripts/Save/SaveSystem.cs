using System.IO;
using UnityEngine;

namespace Save
{
    public class SaveSystem
    {
        private static string saveFilePath;
        private static int currentSlot = 0;
        
        public static void SetCurrentSlot(int slot)
        {
            currentSlot = slot;
        }

        public static void SaveCheckpoint(Vector3 position)
        {
            SaveSlot slot = new SaveSlot();
            if (SaveExists())
            {
                try
                {
                    string loadJson = File.ReadAllText(GetSaveFilePath());
                    slot = JsonUtility.FromJson<SaveSlot>(loadJson);
                }
                catch (System.Exception ex)
                {
                    Debug.LogError("Failed to load save file: " + ex.Message);
                    return;
                }
            }

            if (currentSlot >= slot.SaveDataList.Count)
            {
                slot.SaveDataList.Add(new SaveData
                {
                    playerPositionX = position.x,
                    playerPositionY = position.y,
                });
            }
            else
            {
                slot.SaveDataList[currentSlot] = new SaveData
                {
                    playerPositionX = position.x,
                    playerPositionY = position.y,
                };
            }

            try
            {
                string saveJson = JsonUtility.ToJson(slot);
                File.WriteAllText(GetSaveFilePath(), saveJson);
                Debug.Log("Checkpoint saved.");
            }
            catch (System.Exception ex)
            {
                Debug.LogError("Failed to save checkpoint: " + ex.Message);
            }
        }

        public static Vector3 LoadCheckpoint()
        {
            if (!SaveExists())
            {
                return Vector3.zero;
            }

            try
            {
                string json = File.ReadAllText(GetSaveFilePath());
                SaveSlot slot = JsonUtility.FromJson<SaveSlot>(json);
                if (slot.SaveDataList.Count == 0 || currentSlot >= slot.SaveDataList.Count)
                {
                    return Vector3.zero;
                }

                SaveData data = slot.SaveDataList[currentSlot];
                Vector3 position = new Vector3(data.playerPositionX, data.playerPositionY);
                return position;
            }
            catch (System.Exception ex)
            {
                Debug.LogError("Failed to load checkpoint: " + ex.Message);
                return Vector3.zero;
            }
        }

        public static bool SaveExists()
        {
            return File.Exists(GetSaveFilePath());
        }
        
        private  static string GetSaveFilePath()
        {
            if (!string.IsNullOrEmpty(saveFilePath))
                return saveFilePath;
            
            var saveDirectory = Path.Combine(Application.persistentDataPath, "saves");
            if (!Directory.Exists(saveDirectory))
            {
                Directory.CreateDirectory(saveDirectory);
            }

            saveFilePath = Path.Combine(saveDirectory, "savefile.json");
            return saveFilePath;
        }
    }
}