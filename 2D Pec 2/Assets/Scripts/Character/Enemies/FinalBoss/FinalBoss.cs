using System.Collections.Generic;
using Character.Life;
using DG.Tweening;
using Game;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Character.Enemies.FinalBoss
{
    public class FinalBoss: MonoBehaviour
    {
        [SerializeField] private List<GameObject> _spawnObjects;
        [SerializeField] private List<Transform> _spawnPoints;
        [SerializeField] private GameObject _explosionPrefab;
        [SerializeField] private LifeController _lifeController;
        [SerializeField] private GeneralGameController _generalGameController;
        
        private bool _isOnBattle = false;
        private float _lastSpawnTime;
        private bool _boosMoveIsDown = false;

        private void Awake()
        {
            _lifeController.OnDeath += OnDeath;
        }

        private void OnDeath()
        {
            foreach (var spawnPoint in _spawnPoints)
            {
                Instantiate(_explosionPrefab, spawnPoint.position, Quaternion.identity);
            }
            
            Instantiate(_explosionPrefab, transform.position, Quaternion.identity).transform.localScale *= 3;
            transform.DOScale(Vector3.zero, 2).OnComplete(_generalGameController.OnVictory);
            Destroy(gameObject,2.3f);
        }

        public void StartBattle()
        {
            var battlePosition = transform.position;
            battlePosition.y -= 4;
            transform.DOMove(battlePosition, 2f).OnComplete(() =>
            {
                _isOnBattle = true;
                _lastSpawnTime = Time.time;
                DoBossMovement();
            });
        }

        private void DoBossMovement()
        {
            if (!_boosMoveIsDown)
            {
                transform.DOMove(transform.position + Vector3.down * 0.5f, 0.7f).OnComplete(DoBossMovement);
                _boosMoveIsDown = true;
            }
            else
            {
                _boosMoveIsDown = false;
                transform.DOMove(transform.position + Vector3.up * 0.5f, 0.7f).OnComplete(DoBossMovement);
            }
        }

        private void Update()
        {
            if (!_isOnBattle)
                return;
            
            if (Time.time - _lastSpawnTime > 2)
            {
                _lastSpawnTime = Time.time;
                SpawnRandomObject();
            }
        }

        private void SpawnRandomObject()
        {
            var randomIndex = Random.Range(0, _spawnObjects.Count);
            var randomPosition = Random.Range(0, _spawnPoints.Count);
            Instantiate(_spawnObjects[randomIndex], _spawnPoints[randomPosition].position, Quaternion.identity);
        }
    }
}