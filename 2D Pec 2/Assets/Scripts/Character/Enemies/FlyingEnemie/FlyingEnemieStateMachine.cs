using System;
using Character.Life;
using Character.Utillities;
using DG.Tweening;
using UnityEngine;

namespace Character.Enemies.WalkingEnemy
{
    public class FlyingEnemyStateMachine : StateMachine.StateMachine
    {
        [SerializeField] private TriggerDetector _attackTriggerDetector;
        [SerializeField] private LayerMask _attackEnemyLayer;
        [SerializeField] private TriggerDetector _enemyDetector;

        private GameObject _targetEnemy;
        private Vector2 _direction = new Vector2();
        private bool _isAttacking = false;
        private bool _isTryingToAttack;
        private float _lasAttackTime;
        private float _attackCooldown = 3.5f;
        
        protected override void Awake()
        {
            base.Awake();
            _attackTriggerDetector.OnEnter += OnEnterAttackTrigger;
            _enemyDetector.OnEnter += EnemyDetected;
            _enemyDetector.OnExit += EnemyLost;
        }

        private void OnEnterAttackTrigger(GameObject obj)
        {
            if (IsDeath)return;
            if (obj.TryGetComponent(out LifeController lifeController))
            {
                lifeController.TakeDamage(1);
            }
        }

        public void EnemyDetected(GameObject enemy)
        {
            if (_targetEnemy == enemy)
                return;
            
            _targetEnemy = enemy;
            _isAttacking = true;
            _lasAttackTime = Time.time;
        }
        
        public void EnemyLost(GameObject enemy)
        {
            _isAttacking = false;
            _targetEnemy = null;
            _direction = Vector2.zero;
        }

        public override Vector2 GetMovementInput()
        {
            return _direction;
        }

        public override bool IsJumpTriggered()
        {
            return false;
        }

        public override bool IsJumpReleased()
        {
            return false;
        }

        protected override void Update()
        {
            base.Update();
            UpdateFollowTarget();
        }

        private void UpdateFollowTarget()
        {
            if (!_isAttacking)
                return;
            if (_isTryingToAttack)
                return;
            var defensivePosition = _targetEnemy.transform.position + new Vector3(0, 2.3f, 0);
            var enemyIsAtTheRight = _targetEnemy.transform.position.x > transform.position.x;
            defensivePosition.x += enemyIsAtTheRight ? -1.5f : 1.5f;
            _direction = (defensivePosition - transform.position).normalized;
            
            if (Time.time - _lasAttackTime > _attackCooldown)
            {
                _lasAttackTime = Time.time;
                _isTryingToAttack = true;
                var attackPosition = _targetEnemy.transform.position;
                var finishPosition =defensivePosition + (enemyIsAtTheRight? new Vector3(3f, 0, 0) : new Vector3(-3f, 0, 0));
                transform.DOMove(attackPosition, 0.7f).OnComplete(() =>
                {
                    transform.DOMove(finishPosition, 0.7f).OnComplete(() =>
                    {
                        _isTryingToAttack = false;
                        _lasAttackTime = Time.time;
                    });
                });
            }
        }

        private bool ReferenceExist( object objectToCheck)
        {
            if (objectToCheck == null)
            {
                DebugError ("Check if all references are set.");
                return false;
            }

            return true;
        }

        private void OnDestroy()
        {
            transform.DOKill();
        }
    }
}