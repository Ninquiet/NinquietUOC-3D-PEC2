using Character.Life;
using Character.Utillities;
using UnityEngine;

namespace Character.Enemies.WalkingEnemy
{
    public class EnemyStateMachine : StateMachine.StateMachine
    {
        [SerializeField] private Collider2D _leftCollider;
        [SerializeField] private Collider2D _rightCollider;
        [SerializeField] private Collider2D _topCollider;
        [SerializeField] private TriggerDetector _attackTriggerDetector;
        [SerializeField] private LayerMask _attackEnemyLayer;
        [SerializeField] private bool jumpFollowingPlayer = false;

        private Vector2 _direction = new Vector2();
        
        protected override void Awake()
        {
            base.Awake();
            _attackTriggerDetector.OnEnter += OnEnterAttackTrigger;
        }

        private void OnEnterAttackTrigger(GameObject obj)
        {
            if (IsDeath)return;
            if (obj.TryGetComponent(out LifeController lifeController))
            {
                lifeController.TakeDamage(1);
            }
        }

        public override Vector2 GetMovementInput()
        {
            return _direction;
        }

        public override bool IsJumpTriggered()
        {
            return false;
        }

        public override bool IsJumpReleased()
        {
            return false;
        }

        protected override void Update()
        {
            base.Update();
            
            if (!ReferenceExist(_leftCollider) || !ReferenceExist(_rightCollider))
                return;
            
            _direction = Vector2.zero;
            
            FollowPlayerIfTouching();
        }

        private bool FollowPlayerIfTouching()
        {
            var leftIsTouchingPlayer = _leftCollider.IsTouchingLayers(_attackEnemyLayer);
            var rightIsTouchingPlayer = _rightCollider.IsTouchingLayers(_attackEnemyLayer);
            var topIsTouchingPlayer = _topCollider.IsTouchingLayers(_attackEnemyLayer);
            if (topIsTouchingPlayer)
            {
                JumpIfGrounded(9);
            }
            if (leftIsTouchingPlayer)
            {
                _direction = Vector2.left;
                if (jumpFollowingPlayer)
                {
                    _direction = Vector2.left *1.5f;
                    JumpIfGrounded(6);
                }
                
                return true;
            }
            if (rightIsTouchingPlayer)
            {
                _direction = Vector2.right;
                if (jumpFollowingPlayer)
                {
                    _direction = Vector2.right *1.5f;
                    JumpIfGrounded(5);
                }
                
                return true;
            }

            return false;
        }
        
        private void JumpIfGrounded(float jumpForce)
        {
            if (IsGrounded())
            {
                Jump (jumpForce);
            }
        }

        private bool ReferenceExist( object objectToCheck)
        {
            if (objectToCheck == null)
            {
                DebugError ("Check if all references are set.");
                return false;
            }

            return true;
        }
    }
}