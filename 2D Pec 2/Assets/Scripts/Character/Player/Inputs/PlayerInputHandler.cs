using System;
using UnityEngine;

namespace Character.Player
{
    public abstract class PlayerInputHandler : MonoBehaviour
    {
        public abstract Vector2 GetMovementInput();
        public abstract bool IsJumpTriggered();
        public abstract bool IsJumpReleased();
        public abstract Vector2 GetShootingDirection();
        
        public abstract bool IsShootingPressed();
        
        public abstract bool IsShootingReleased();
    }
}