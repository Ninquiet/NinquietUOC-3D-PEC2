using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TouchPhase = UnityEngine.TouchPhase;

namespace Character.Player
{
    public class TouchInputHandler : PlayerInputHandler
    {
        [SerializeField] 
        public RectTransform _rightTouchPadCircleOnScreenCanvas;
        [SerializeField]
        public RectTransform _leftTouchPadCircleOnScreenCanvas;
        [SerializeField]
        public Button _jumpButton;

        private Vector2 _shootingDirection;
        private bool _isShootingPressed;
        private bool _isShootingReleased;
        private bool _isShootingPressedThisFrame;
        private bool _isJumpTriggered;
        private bool _isJumpReleased;
        private Camera _mainCamera;
        private Vector2 _currentMovementInput;
        private Vector2 _startTouchPosition;
        private Vector2 _startRightTouchPosition;
        private int _activeMovementTouchId = -1;
        private int _activeShootingTouchId = -1;
        private Vector3 _offScreenPosition = new Vector3(-1000, -1000, 0);

        private void Awake()
        {
            _mainCamera = Camera.main;
            _shootingDirection = Vector2.zero;
            _isShootingPressed = false;
            _isShootingReleased = false;

            AddEventTrigger(_jumpButton.gameObject, EventTriggerType.PointerDown, OnJumpButtonPressed);
            AddEventTrigger(_jumpButton.gameObject, EventTriggerType.PointerUp, OnJumpButtonReleased);
            
            _rightTouchPadCircleOnScreenCanvas.position = _offScreenPosition;
            _leftTouchPadCircleOnScreenCanvas.position = _offScreenPosition;
        }

        private void Update()
        {
            HandleTouchInput();
            _isShootingPressedThisFrame = _isShootingPressed; 
            _isShootingPressed = false; 
        }

        private void HandleTouchInput()
        {
            _currentMovementInput = Vector2.zero;
            bool leftTouchActive = false;
            bool rightTouchActive = false;

            foreach (Touch touch in Input.touches)
            {
                if (!RectTransformUtility.RectangleContainsScreenPoint(_jumpButton.GetComponent<RectTransform>(), touch.position, _mainCamera))
                {
                    if (touch.position.x < Screen.width / 2)
                    {
                        leftTouchActive = true;
                        _leftTouchPadCircleOnScreenCanvas.position = touch.position;

                        if (touch.phase == TouchPhase.Began)
                        {
                            _startTouchPosition = touch.position;
                            _activeMovementTouchId = touch.fingerId;
                        }

                        if (touch.fingerId == _activeMovementTouchId)
                        {
                            if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
                            {
                                _currentMovementInput = GetTouchMovement(touch);
                            }
                            else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                            {
                                _activeMovementTouchId = -1;
                            }
                        }
                    }
                    else if (touch.position.x >= Screen.width / 2 && !_isJumpTriggered)
                    {
                        rightTouchActive = true;
                        _rightTouchPadCircleOnScreenCanvas.position = touch.position;

                        if (touch.phase == TouchPhase.Began)
                        {
                            _startRightTouchPosition = touch.position;
                            _activeShootingTouchId = touch.fingerId;
                            _isShootingPressed = true;
                            _isShootingReleased = false;
                        }
                        else if (touch.fingerId == _activeShootingTouchId)
                        {
                            if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
                            {
                                _shootingDirection = GetTouchShootingDirection(touch);
                            }
                            else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                            {
                                _isShootingReleased = true;
                                _isShootingPressed = false;
                                _activeShootingTouchId = -1;
                            }
                        }
                    }
                }
            }

            if (!leftTouchActive)
            {
                _leftTouchPadCircleOnScreenCanvas.position = _offScreenPosition;
            }
            if (!rightTouchActive)
            {
                _rightTouchPadCircleOnScreenCanvas.position = _offScreenPosition;
            }
        }

        private Vector2 GetTouchMovement(Touch touch)
        {
            return (touch.position - _startTouchPosition).normalized;
        }

        private Vector2 GetTouchShootingDirection(Touch touch)
        {
            return (touch.position - _startRightTouchPosition).normalized;
        }

        private void OnJumpButtonPressed()
        {
            _isJumpTriggered = true;
            _isJumpReleased = false;
        }

        private void OnJumpButtonReleased()
        {
            _isJumpTriggered = false;
            _isJumpReleased = true;
        }

        private void AddEventTrigger(GameObject obj, EventTriggerType type, Action callback)
        {
            EventTrigger trigger = obj.GetComponent<EventTrigger>();
            if (trigger == null)
            {
                trigger = obj.AddComponent<EventTrigger>();
            }
            var entry = new EventTrigger.Entry { eventID = type };
            entry.callback.AddListener((eventData) => callback());
            trigger.triggers.Add(entry);
        }

        public override Vector2 GetMovementInput()
        {
            return _currentMovementInput;
        }

        public override bool IsJumpTriggered()
        {
            return _isJumpTriggered;
        }

        public override bool IsJumpReleased()
        {
            bool jumpReleaseState = _isJumpReleased;
            _isJumpReleased = false;
            return jumpReleaseState;
        }

        public override Vector2 GetShootingDirection()
        {
            Vector2 result = (Vector2)transform.position + _shootingDirection;
            return result;
        }

        public override bool IsShootingPressed()
        {
            return _isShootingPressedThisFrame;
        }

        public override bool IsShootingReleased()
        {
            return _isShootingReleased;
        }
    }
}
