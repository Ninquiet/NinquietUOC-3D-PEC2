using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Character.Player
{
    public class KeyboardAndMouseInputHandler : PlayerInputHandler
    {
        [SerializeField] private InputActionReference _movementInput;
        [SerializeField] private InputActionReference _jumpInput;
        [SerializeField] private InputActionReference _shootInput;
        [SerializeField] private InputActionReference _mousePositionInput;
        
        private Camera _mainCamera;
        
        private void Awake()
        {
            _mainCamera = Camera.main;
        }
        public override Vector2 GetMovementInput()
        {
            return _movementInput.action.ReadValue<Vector2>();
        }

        public override bool IsJumpTriggered()
        {
            return _jumpInput.action.triggered;
        }

        public override bool IsJumpReleased()
        {
            return _jumpInput.action.phase == InputActionPhase.Waiting;
        }

        public override Vector2 GetShootingDirection()
        {
            var mousePosition = _mousePositionInput.action.ReadValue<Vector2>();
            var worldPosition = MouseToWorld(mousePosition);
            return new Vector2(worldPosition.x, worldPosition.y);
        }

        public override bool IsShootingPressed()
        {
            return _shootInput.action.triggered;
        }

        public override bool IsShootingReleased()
        {
            return _shootInput.action.phase == InputActionPhase.Waiting;
        }
        
        private Vector3 MouseToWorld(Vector2 mousePosition)
        {
            Vector3 worldPosition =
                _mainCamera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y,
                    Camera.main.nearClipPlane));
            return worldPosition;
        }
    }
}