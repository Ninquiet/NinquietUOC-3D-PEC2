using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Character.Player
{
    public class PlayerInputs : MonoBehaviour
    {
        [SerializeField] 
        private GameObject _touchUI;
        [SerializeField] 
        private PlayerInputHandler _keyboardInputHandler;
        [SerializeField] 
        private PlayerInputHandler _touchPadInputHandler;

        private PlayerInputHandler _currentInputHandler;

        public Vector2 GetMovementInput()
        {
            return _currentInputHandler.GetMovementInput();
        }

        public bool IsJumpTriggered()
        {
            return _currentInputHandler.IsJumpTriggered();
        }

        public bool IsJumpReleased()
        {
            return _currentInputHandler.IsJumpReleased();
        }

        public Vector2 GetShootingDirection()
        {
            return _currentInputHandler.GetShootingDirection();
        }

        public bool IsShootingPressed()
        {
            return _currentInputHandler.IsShootingPressed();
        }

        public bool IsShootingReleased()
        {
            return _currentInputHandler.IsShootingReleased();
        }

        private void Awake()
        {
            _currentInputHandler = _keyboardInputHandler;
        }

        private void Update()
        {
            DetectInputDevice();
        }

        private void DetectInputDevice()
        {
            if (Touchscreen.current != null && Touchscreen.current.primaryTouch.press.isPressed)
            {
                if (_currentInputHandler != _touchPadInputHandler)
                {
                    _currentInputHandler = _touchPadInputHandler;
                    _touchUI.SetActive(true);
                }
            }
            
            if (Keyboard.current.anyKey.isPressed || Mouse.current.leftButton.isPressed || Mouse.current.rightButton.isPressed)
            {
                if (_currentInputHandler != _keyboardInputHandler)
                {
                    _currentInputHandler = _keyboardInputHandler;
                    _touchUI.SetActive(false);
                }
            }
        }
    }
}
