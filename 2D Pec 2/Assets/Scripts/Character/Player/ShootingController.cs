using System;
using Bullets;
using DG.Tweening;
using Sound;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Character.Player
{
    public enum ShootState
    {
        Idle,
        Targeting,
    }

    public class ShootingController : MonoBehaviour
    {
        [SerializeField] private GameObject _projectilePrefab;
        [SerializeField] private GameObject _GunGameobject;
        [SerializeField] private SpriteRenderer _GunSpriteRenderer;
        [SerializeField] private Transform _powerScaleUI;
        [SerializeField] private LayerMask _enemyLayer;
        [SerializeField] private ItemInventory _itemInventory;
        [SerializeField] private PlayerInputs _playerInputs;

        private Vector2 _targetDirection;
        private bool _canShoot;
        private ShootState _shootState = ShootState.Idle;
        private Camera _mainCamera;
        private float _shootMultiplier = 1f;
        private float _maxMultiplier = 4f;
        private float _minMultiplier = 1f;
        private float _multiplierPerSecond = 4f;
        private bool _multiplierGoingUp = true;
        private float _multiplierLastChangedTime;
        private float _reloadTime = 0.5f;
        private float _lastTimeShot;
        private bool _isReloading;
        private bool _isOutOfMunitions;
        
        public Action OnShoot;
        public Action OnCantShoot;

        private void Awake()
        {
            _itemInventory.OnMunitionsChanged += MunitionChanged;
            _mainCamera = Camera.main;
        }

        public void SetCanShoot(bool canShoot)
        {
            _canShoot = canShoot;
        }

        private void Update()
        {
            UpdateTargeting();
            CheckInputs();
        }

        private void CheckInputs()
        {
            if (_playerInputs.IsShootingPressed())
            {
                ShootingPressed();
            }
            if (_playerInputs.IsShootingReleased())
            {
                ShootingReleased();
            }
        }

        private void ShootingPressed()
        {
            if (!_canShoot) 
                return;
            if (_itemInventory.GetMunitions()<=0)
            {
                OnCantShoot?.Invoke();
                return;
            }

            UpdateShootDirection();
            _shootMultiplier = 1f;
            _multiplierGoingUp = true;
            _shootState = ShootState.Targeting;
        }

        private void ShootingReleased()
        {
            if (_shootState == ShootState.Targeting)
            {
                ShootProjectile();
                _shootState = ShootState.Idle;
            }
        }

        private void UpdateShootDirection()
        {
            _targetDirection = _playerInputs.GetShootingDirection();
        }

        private void UpdateTargeting()
        {
            if (_shootState == ShootState.Targeting)
            {
                _powerScaleUI.gameObject.SetActive(!_isReloading);

                UpdateShootDirection();
                UpdateShootingMultiplier();
                UpdateGunRotation();
                UpdatePowerScaleUI();
            }
            else
            {
                _powerScaleUI.gameObject.SetActive(false);
            }
        }

        private void UpdatePowerScaleUI()
        {
            _powerScaleUI.localScale = new Vector3(_shootMultiplier / _maxMultiplier, 7, 1);
        }

        private void UpdateGunRotation()
        {
            var direction = _targetDirection - (Vector2)_GunGameobject.transform.position;
            var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            _GunGameobject.transform.rotation = Quaternion.Euler(0, 0, angle);
        }

        private void UpdateShootingMultiplier()
        {
            if (_isReloading)
            {
                _shootMultiplier = _minMultiplier;
                return;
            }

            if (_multiplierGoingUp)
            {
                _shootMultiplier += Time.deltaTime * _multiplierPerSecond;
                if (_shootMultiplier >= _maxMultiplier)
                {
                    _shootMultiplier = _maxMultiplier;
                    _multiplierGoingUp = false;
                }
            }
            else
            {
                _shootMultiplier -= Time.deltaTime * _multiplierPerSecond;
                if (_shootMultiplier <= _minMultiplier)
                {
                    _shootMultiplier = _minMultiplier;
                    _multiplierGoingUp = true;
                }
            }
        }

        private void ShootProjectile()
        {
            if (_projectilePrefab == null)
                return;
            if (_isReloading)
                return;
            if (Time.time - _lastTimeShot < _reloadTime)
                return;
            if (_itemInventory.GetMunitions() <= 0)
                return;
            
            _itemInventory.RemoveMunitions(1);
            var projectile = Instantiate(_projectilePrefab, _GunGameobject.transform.position, Quaternion.identity);
            var basicBullet = projectile.GetComponent<BasicBullet>();
            basicBullet.Initialize(_targetDirection, _enemyLayer, _shootMultiplier);
            _lastTimeShot = Time.time;
            GameSoundHandler.Instance.PlayAudio(Sound.AudioType.PlayerShoot);
            OnShoot?.Invoke();
            Reload();
        }

        private void Reload()
        {
            _isReloading = true;
            _GunSpriteRenderer.DOColor(Color.red, 0.1f);
            var reloadScale = new Vector3(0.2f, 1, 1);
            _GunGameobject.transform.DOScale(reloadScale, _reloadTime / 2).OnComplete(() => { TryToReload(); });
        }

        private void TryToReload()
        {
            if (_itemInventory.GetMunitions() <= 0)
            {
                _isOutOfMunitions = true;
                return;
            }
            
            _GunGameobject.transform.DOKill();
            _GunGameobject.transform.DOScale(Vector3.one, _reloadTime / 2).OnComplete(() =>
            {
                _GunSpriteRenderer.DOColor(Color.cyan, 0.1f);
                _isReloading = false;
            });
        }
        
        private void MunitionChanged()
        {
            if (_isOutOfMunitions)
                if (_itemInventory.GetMunitions() > 0)
                {
                    _isOutOfMunitions = false;
                    TryToReload();
                }
        }
    }
}