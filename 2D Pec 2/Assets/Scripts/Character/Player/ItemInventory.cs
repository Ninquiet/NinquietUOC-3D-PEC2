using System;
using UnityEngine;

namespace Character.Player
{
    public class ItemInventory: MonoBehaviour
    {
        private int _munitions = 5;
        
        public Action OnMunitionsChanged;
        
        public void AddMunitions(int munitions)
        {
            _munitions += munitions;
            OnMunitionsChanged?.Invoke();
        }
        
        public void RemoveMunitions(int munitions)
        {
            _munitions -= munitions;
            OnMunitionsChanged?.Invoke();
        }

        public int GetMunitions()
        {
            return _munitions;
        }
    }
}