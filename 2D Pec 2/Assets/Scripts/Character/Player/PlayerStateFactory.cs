using Character.StateMachine;

namespace Character.Player
{
    public class PlayerStateFactory : StateFactory
    {
        public PlayerStateFactory(StateMachine.StateMachine ctx) : base(ctx)
        {
        }
    }
}