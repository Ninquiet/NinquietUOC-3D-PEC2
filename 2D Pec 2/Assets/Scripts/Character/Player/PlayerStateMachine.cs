using System.Collections.Generic;
using Character.Life;
using Character.StateMachine;
using Character.Utillities;
using DG.Tweening;
using Sound;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Character.Player
{
    public class PlayerStateMachine : StateMachine.StateMachine
    {
        [SerializeField] private LayerMask _attackEnemyLayer;
        [SerializeField] private TriggerDetector _attackTriggerDetector;
        [SerializeField] private GameObject _playerHat;
        [SerializeField] private SpriteRenderer _playerSpriteRenderer;
        [SerializeField] private PlayerInputs _playerInputs;

        private List<GameObject> _enemiesThatAreSteppedOn = new();
        private StateFactory _stateFactory;
        private Vector3 _hatOriginalLocalPosition;
        private int _currentLifePoints = 1;

        protected override void Awake()
        {
            base.Awake();
            _stateFactory = new PlayerStateFactory(this);
            _hatOriginalLocalPosition = _playerHat.transform.localPosition;
            SetStateFactory(_stateFactory);
            _attackTriggerDetector.OnEnter += OnEnterAttackTrigger;
            GetShootingController.OnShoot += OnShoot;
            GetLifeController.OnLifePointsChanged += OnLifePointsChanged;
        }

        public override Vector2 GetMovementInput()
        {
            return _playerInputs.GetMovementInput();
        }

        public override bool IsJumpTriggered()
        {
            return _playerInputs.IsJumpTriggered();
        }

        public override bool IsJumpReleased()
        {
            return _playerInputs.IsJumpReleased();
        }

        private void OnEnterAttackTrigger(GameObject obj)
        {
            _enemiesThatAreSteppedOn.Add(obj);
        }

        protected override void Update()
        {
            CheckStepOnEnemy();
            base.Update();
        }

        private void CheckStepOnEnemy()
        {
            if (_enemiesThatAreSteppedOn == null || _enemiesThatAreSteppedOn.Count == 0 || IsDeath)
                return;
            
            Jump(3);

            foreach (var enemy in _enemiesThatAreSteppedOn)
            {
                var lifeController = enemy.GetComponent<LifeController>();
                if (lifeController == null)
                    continue;
                lifeController.TakeDamage(1);
            }

            _enemiesThatAreSteppedOn.Clear();
        }
        
        private void OnShoot()
        {
            Jump(4);
        }

        protected override void OnDeath()
        {
            base.OnDeath();
            GameSoundHandler.Instance.PlayAudio(Sound.AudioType.PlayerDie);
        }

        private void OnLifePointsChanged(int value)
        {
            if (_currentLifePoints == value)
                return;

            GetLifeController.SetCanGetDamage(false);
            _playerSpriteRenderer.DOKill();
            
            if (value == 2)
            {
                _playerSpriteRenderer.DOColor(new Color(0.7f, 0.9f, 0.5f, 1), 0.5f).OnComplete(() =>
                {
                    GetLifeController.SetCanGetDamage(true);
                });
                _playerHat.transform.parent = transform;
                _playerHat.transform.localPosition = _hatOriginalLocalPosition;
                _playerHat.SetActive(true);
            }

            if (value == 1)
            {
                _playerSpriteRenderer.DOColor(new Color(1, 1f, 1f, 1), 0.5f).OnComplete(() =>
                {
                    GetLifeController.SetCanGetDamage(true);
                });
                if (_currentLifePoints == 2)
                {
                    _playerHat.transform.parent = null;
                    var hatPosition = _playerHat.transform.position + Vector3.down * 4f;
                    hatPosition.z = -1;
                    _playerHat.transform.DOLocalMove( hatPosition, 1f).OnComplete(() =>
                    {
                        _playerHat.SetActive(false);
                    });
                }
            }
            _currentLifePoints = value;
        }
    }
}