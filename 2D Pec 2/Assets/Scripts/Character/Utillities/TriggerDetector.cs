using System;
using UnityEngine;
using UnityEngine.Events;

namespace Character.Utillities
{
    public class TriggerDetector : MonoBehaviour
    {
        [SerializeField] private string _tag;
        
        public Action<GameObject> OnEnter;
        public Action<GameObject> OnExit;
        public UnityEvent OnEnterEvent;
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(_tag))
            {
                OnEnter?.Invoke(other.gameObject);
                OnEnterEvent?.Invoke();
            }
        }
        
        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag(_tag))
            {
                OnExit?.Invoke(other.gameObject);
            }
        }
    }
}