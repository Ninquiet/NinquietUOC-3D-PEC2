using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Character
{
    public class CharacterMovement : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D _rB;
        [SerializeField] private float _speed = 5;
        [SerializeField] private float _jumpForce = 5;

        private float _horizontalDir;
        private bool _characterOnJump;
        private bool _lastTimeWasFacingRight;
        
        public Action OnMovement;
        public Action OnJump;
        public Action<bool> OnGrounded;
        public Action OnStay;
        
        public void Move (InputAction.CallbackContext context)
        {
            _horizontalDir = context.ReadValue<Vector2>().x; 
        }

        public void Jump(InputAction.CallbackContext context)
        {
            // if (context.performed && IsGrounded())
            // {
            //     _characterOnJump = true;
            //     _rB.velocity = new Vector2(_rB.velocity.x, _jumpForce);
            // }

            if (context.canceled && _rB.velocity.y > 0f && _characterOnJump)
            {
                _rB.velocity = new Vector2(_rB.velocity.x, _rB.velocity.y * 0.5f);
            }
        }
    
        private void Update()
        {
            var isFacingToTheRight = _horizontalDir > 0;
            FaceToTheRight(_horizontalDir == 0 ? _lastTimeWasFacingRight : isFacingToTheRight);

            _rB.velocity = new Vector2(_horizontalDir * _speed, _rB.velocity.y);
        }

        private void FaceToTheRight(bool faceToTheRight)
        {
            _lastTimeWasFacingRight = faceToTheRight;
        
            Vector3 newLocalScale = transform.localScale;
            var baseXScale = Mathf.Abs(newLocalScale.x);
        
            newLocalScale.x = faceToTheRight? baseXScale :  baseXScale * -1f;
        
            transform.localScale = newLocalScale;
        }
    }
}