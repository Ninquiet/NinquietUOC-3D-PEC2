using System;
using Sound;
using UnityEngine;
using UnityEngine.Events;
using AudioType = UnityEngine.AudioType;

namespace Character.Life
{
    public class LifeController : MonoBehaviour
    {
        [SerializeField] int _lifePoints = 1;
        [SerializeField] int _maxLifePoints = 2;
        
        public Action<int> OnLifePointsChanged;
        public Action OnBeingHit;
        public Action OnDeath;

        public UnityEvent OnDeathEvent;
        
        private bool _isCanGetDamage = true;
        
        public void SetMaxLifePoints(int maxLifePoints)
        {
            _maxLifePoints = maxLifePoints;
        }
        
        public void SetLifePoints(int lifePoints)
        {
            _lifePoints = lifePoints;
            OnLifePointsChanged?.Invoke(lifePoints);
        }
        
        public void SetCanGetDamage(bool canGetDamage)
        {
            _isCanGetDamage = canGetDamage;
        }
        
        public void TakeDamage(int damage)
        {
            if (!_isCanGetDamage) 
                return;
            _lifePoints -= damage;
            if (_lifePoints <= 0)
            {
                _lifePoints = 0;
                OnDeath?.Invoke();
                OnDeathEvent?.Invoke();
                GameSoundHandler.Instance.PlayAudio(Sound.AudioType.EnemyDie);
            }
            OnBeingHit?.Invoke();
            OnLifePointsChanged?.Invoke(_lifePoints);
            GameSoundHandler.Instance.PlayAudio(Sound.AudioType.PlayerHit);
        }
        
        public void Heal(int heal)
        {
            if (_lifePoints >= _maxLifePoints) 
                return;
            _lifePoints += heal;
            OnLifePointsChanged?.Invoke(_lifePoints);
            GameSoundHandler.Instance.PlayAudio(Sound.AudioType.PlayerHeal);
        }
    }
}