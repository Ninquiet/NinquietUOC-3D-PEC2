using System;
using DG.Tweening;
using UnityEngine;

namespace Character.StateMachine
{
    public class CharacterAnimationController : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private Transform _characterTransform;

        public void UpdateFacingDirection(Vector2 direction)
        {
            var xScale = _characterTransform.localScale.x;
            var yScale = _characterTransform.localScale.y;
            if (direction.x > 0)
            {
                _characterTransform.localScale = new Vector3(Mathf.Abs(xScale), yScale, 1);
            }
            else if (direction.x < 0)
            {
                _characterTransform.localScale = new Vector3(-Mathf.Abs(xScale), yScale, 1);
            }
        }
        
        public void SetFloat(string name, float value)
        {
            _animator.SetFloat(name, value);
        }
        
        public void SetBool(string name, bool value)
        {
            _animator.SetBool(name, value);
        }
        
        public void SetTrigger(string name)
        {
            _animator.SetTrigger(name);
        }
        
        public void ResetTrigger(string name)
        {
            _animator.ResetTrigger(name);
        }
        
        public void DoDeath(Action onComplete)
        {
            var initialScale = _characterTransform.localScale;
            initialScale.y *= 0.5f;
            var finalScale = _characterTransform.localScale * 0.1f;
            _characterTransform.DOScale(initialScale, 0.1f).OnComplete(() =>
            {
                _characterTransform.DOScale(finalScale, 0.4f); 
            });
            
           _characterTransform.DORotate(new Vector3(0, 0, 180), 0.6f);
    
            Vector3[] path = new Vector3[] {
                _characterTransform.position,
                _characterTransform.position + new Vector3(0, -5, -1)
            };
            _characterTransform.DOPath(path, 0.6f, PathType.CatmullRom, PathMode.TopDown2D)
                .SetEase(Ease.InOutSine).OnComplete(() =>
                {
                    onComplete?.Invoke();
                }); 
        }
        
        private void Awake()
        {
            if (_animator == null)
            {
                throw new NullReferenceException($"Animator is not set on {gameObject.name}");
            }
        }
    }
}