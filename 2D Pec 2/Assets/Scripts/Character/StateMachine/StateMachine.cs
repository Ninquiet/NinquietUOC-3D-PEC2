using Character.Life;
using Character.Player;
using Sound;
using UnityEngine;

namespace Character.StateMachine
{
    public abstract class StateMachine : MonoBehaviour
    {
        [SerializeField] private Transform groundCheckTransform;
        [SerializeField] private LayerMask _groundLayer;
        [SerializeField] private Rigidbody2D _rB;
        [SerializeField] private Collider2D _mainCollider;
        [SerializeField] private CharacterAnimationController AnimationController;
        [SerializeField] private ShootingController _shootingController;
        [SerializeField] private LifeController _lifeController;
        [SerializeField] private bool _isFlyingEnemy = false; 
        
        public float Speed = 5 ;
        public float JumpForce = 10;
        
        public bool DoDebug = false;
        
        private Vector3 _velocity;
        private StateFactory _stateFactory;
        private BaseState _currentState;
        private BaseState _baseState;
        private bool _isJumping = false;
        private bool _isFalling = false;
        private float _jumpingTime;
        private bool _isGroundedCacheSaved = false;
        private bool _isGroundedCache = false;
        private float _landingTime = 0.1f;
        private float _jumpStartingTime = 0.1f;
        private float _notGroundedTime =0.05f;
        private bool _isDeath;
        
        public Vector3 Velocity { get => _velocity; set => _velocity = value; }
        public StateFactory StateFactory => _stateFactory;
        public void SetStateFactory(StateFactory stateFactory) => _stateFactory = stateFactory;
        public bool IsJumping {get => _isJumping; set { SetJumpingValue(value); } }
        public bool IsFalling { get => _isFalling; set => _isFalling = value; }
        public bool CanCheckGrounded => Time.time - _jumpingTime > 0.05f;
        public float GetNotGroundedTime() => _notGroundedTime;
        public Vector2 GetRbVelocity() => _rB.velocity;
        public CharacterAnimationController GetAnimationController => AnimationController;
        public float GetLandingTime() => _landingTime;
        public float GetJumpStatingTime() => _jumpStartingTime;
        public ShootingController GetShootingController => _shootingController;
        public Rigidbody2D GetRigidBody => _rB; 
        public LifeController GetLifeController => _lifeController;
        public Transform GetGroundCheckTransform => groundCheckTransform;
        public bool IsDeath => _isDeath;

        public abstract Vector2 GetMovementInput();
        public abstract bool IsJumpTriggered();
        public abstract bool IsJumpReleased();
        
        public bool IsGrounded()
        {
            if (!ReferenceExist(groundCheckTransform) || !ReferenceExist(_groundLayer))
                return false;
            
            if (!_isGroundedCacheSaved)
            {
                _isGroundedCache = Physics2D.OverlapCircle(groundCheckTransform.position, 0.2f, _groundLayer);
                _isGroundedCacheSaved = true;
            }

            return _isGroundedCache;
        }
        
        public void ChangeState(BaseState state)
        {
            if (DoDebug && _currentState !=null)
                Debug.Log($"from {_currentState.GetType().Name} to {state.GetType().Name}");
            if (_currentState !=null)
                _currentState.OnExitState();
            if (!state.IsRootState)
            {
                DebugError($"Trying to change to a non root state.{state.GetType()} from {_currentState.GetType()}");
                return;
            }
            
            _currentState = state;
            _currentState.OnEnterState();
        }
        
        public void Jump(float jumpForce)
        {
            _rB.velocity = new Vector2(_rB.velocity.x, jumpForce);
            GameSoundHandler.Instance.PlayAudio(Sound.AudioType.PlayerJump);
        }
        
        public void MultiplyYVelocityOnRb(float value)
        {
            _rB.velocity = new Vector2(_rB.velocity.x, _rB.velocity.y * value);
        }
        
        public void CanShoot(bool action)
        {
            if (_shootingController == null)
                return;
            
            _shootingController.SetCanShoot(action);
        }
        
        protected virtual void Awake()
        {
            _stateFactory = new StateFactory(this);
            _lifeController.OnDeath += OnDeath;
        }
        
        protected virtual void OnDeath()
        {
            _isDeath = true;
            _mainCollider.enabled = false;
            _rB.isKinematic = true;
            GetAnimationController.DoDeath(()=>Destroy(gameObject));
        }

        protected void Start()
        {
            Initialize();
        }

        private void Initialize()
        {
            _baseState = _stateFactory.GetBaseState();
            ChangeState(_baseState);
        }

        protected virtual void Update()
        {
            if (!ReferenceExist(_currentState))
                return;
            
            ResetValuesBeforeUpdate();

            _currentState.UpdateStates();
            
            DoMovement();
            UpdateAnimationFacingDirection();
        }

        private void ResetValuesBeforeUpdate()
        {
            _isGroundedCacheSaved = false;
            _velocity = Vector2.zero;
            IsGrounded();
        }

        private void DoMovement()
        {
            if (!ReferenceExist(_rB))
                return;
            
            if (_isFlyingEnemy)
                _rB.velocity = new Vector2(_velocity.x, _velocity.y);
            else
                _rB.velocity = new Vector2(_velocity.x,  _rB.velocity.y);
        }
        
        private void UpdateAnimationFacingDirection()
        {
            AnimationController.UpdateFacingDirection(_rB.velocity);
        }

        private bool ReferenceExist( object objectToCheck)
        {
            if (objectToCheck == null)
            {
                DebugError ("Check if all references are set.");
                return false;
            }

            return true;
        }
        
        private void SetJumpingValue( bool value)
        {
            _isJumping = value; 
            if (value)
                _jumpingTime = Time.time;
        }
        
        public void DebugError(string message)
        {
            Debug.LogError($"ERROR ON -{gameObject.name}- :"+message);
        }
    }
}