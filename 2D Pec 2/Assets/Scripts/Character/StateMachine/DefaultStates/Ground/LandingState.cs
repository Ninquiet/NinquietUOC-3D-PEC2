using UnityEngine;

namespace Character.StateMachine
{
    public class LandingState : BaseState
    {
        private float landingtime = 0;
        public LandingState(string stateName, StateMachine ctx, StateFactory stateFactory) : base(stateName, ctx, stateFactory)
        {
        }

        public override void OnEnterState()
        {
            _ctx.CanShoot(true);
            landingtime = 0;
            _ctx.GetAnimationController.ResetTrigger("IsFalling");
            _ctx.GetAnimationController.SetTrigger("Landing");
            SetSubState(_stateFactory.Idle);
        }

        protected override void Update()
        {
            CheckSwitchStates();
        }

        protected override void CheckSwitchStates()
        {
            if (Time.time - landingtime > _ctx.GetLandingTime())
            {
                SwitchThisState(_stateFactory.Idle);
            }
        }

        public override void OnExitState()
        {
        }
    }
}