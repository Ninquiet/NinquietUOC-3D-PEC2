using UnityEngine;

namespace Character.StateMachine
{
    public class GroundedState : BaseState
    {
        public GroundedState(string stateName, StateMachine ctx, StateFactory stateFactory) : base(stateName, ctx, stateFactory)
        {
            IsRootState = true;
        }

        public override void OnEnterState()
        {
            if (_ctx.IsJumping || _ctx.IsFalling)
                SetSubState(_stateFactory.Landing);
            else
                SetSubState(_stateFactory.Idle);
            
            _ctx.CanShoot(true);
            _ctx.IsFalling = false;
            _ctx.IsJumping = false;
        }

        protected override void Update()
        {
            CheckSwitchStates();
        }

        protected override void CheckSwitchStates()
        {
            var isGrounded = _ctx.IsGrounded();
            if (_ctx.IsJumpTriggered() && isGrounded)
            {
                _ctx.IsJumping = true;
                _ctx.ChangeState(_stateFactory.Jump);
            }

            if (!isGrounded)
            {
                _ctx.ChangeState(_stateFactory.Airborne);
            }
        }

        public override void OnExitState()
        {
        }
    }
}