using UnityEngine;

namespace Character.StateMachine
{
    public class HorizontalMovementState : BaseState
    {
        public HorizontalMovementState(string stateName, StateMachine ctx, StateFactory stateFactory) : base(stateName, ctx, stateFactory)
        {
        }

        public override void OnEnterState()
        {
            _ctx.CanShoot(true);
            _ctx.GetAnimationController.SetTrigger("HorizontalMovement");
            _ctx.GetAnimationController.ResetTrigger("Idle");
        }

        protected override void Update()
        {
            var inputMovement = _ctx.GetMovementInput();
            _ctx.Velocity = inputMovement * _ctx.Speed;
            CheckSwitchStates();
        }

        protected override void CheckSwitchStates()
        {
            var inputMovement = _ctx.GetMovementInput();
            if (inputMovement == Vector2.zero)
            {
                SwitchThisState(_ctx.StateFactory.Idle);
            }
        }

        public override void OnExitState()
        {
        }
        
    }
}