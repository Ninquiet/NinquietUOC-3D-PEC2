using UnityEngine;

namespace Character.StateMachine
{
    public class IdleState : BaseState
    {
        public IdleState(string stateName, StateMachine ctx, StateFactory stateFactory) : base(stateName, ctx, stateFactory)
        {
        }

        public override void OnEnterState()
        {
            _ctx.CanShoot(true);
            _ctx.GetAnimationController.ResetTrigger("HorizontalMovement");
            _ctx.GetAnimationController.SetTrigger("Idle");
        }

        protected override void Update()
        {
            CheckSwitchStates();
        }

        protected override void CheckSwitchStates()
        {
            var inputMovement = _ctx.GetMovementInput();
            if (inputMovement != Vector2.zero)
            {
                SwitchThisState(_ctx.StateFactory.HorizontalMoving);
            }
        }

        public override void OnExitState()
        {
        }
    }
}