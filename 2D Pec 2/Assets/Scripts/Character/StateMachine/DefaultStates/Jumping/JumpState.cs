using UnityEngine;

namespace Character.StateMachine
{
    public class JumpState : BaseState
    {
        private float _jumptStartingTime = 0;
        private float _jumpPerformedTime = 0;
        private bool _isJumping = false;
        public JumpState(string stateName, StateMachine ctx, StateFactory stateFactory) : base(stateName, ctx, stateFactory)
        {
            IsRootState = true;
        }

        public override void OnEnterState()
        {
            _ctx.CanShoot(true);
            SetSubState(_stateFactory.HorizontalMoving);
            _ctx.GetAnimationController.SetTrigger("StartJump");
            _jumptStartingTime = Time.time;
        }

        protected override void Update()
        {
            if (Time.time - _jumptStartingTime > _ctx.GetJumpStatingTime() && !_isJumping)
            {
                _ctx.Jump(_ctx.JumpForce);
                _isJumping = true;
                _jumpPerformedTime = Time.time;
            }
            CheckSwitchStates();
        }

        protected override void CheckSwitchStates()
        {
            if (!_ctx.CanCheckGrounded)
                return;
            
            var waitingTimeIsOff = _jumpPerformedTime + _ctx.GetNotGroundedTime() < Time.time;
            if (_isJumping && waitingTimeIsOff )
                _ctx.ChangeState(_stateFactory.Airborne);
        }

        public override void OnExitState()
        {
        }
    }
}