using UnityEngine;

namespace Character.StateMachine
{
    public class GoingUpState : BaseState

    {
        public GoingUpState(string stateName, StateMachine ctx, StateFactory stateFactory) : base(stateName, ctx, stateFactory)
        {
        }

        public override void OnEnterState()
        {
            SetSubState(_stateFactory.HorizontalMoving);
            
            _ctx.CanShoot(true);
        }

        protected override void Update()
        {
            CheckSwitchStates();
        }

        protected override void CheckSwitchStates()
        {
            if (_ctx.CanCheckGrounded && _ctx.IsGrounded())
            {
                _ctx.ChangeState(_stateFactory.Grounded);
            }
            if (_ctx.IsJumpReleased() || _ctx.GetRbVelocity().y < 0)
            {
                SwitchThisState(_stateFactory.Falling);
            }
        }

        public override void OnExitState()
        {
        }
    }
}