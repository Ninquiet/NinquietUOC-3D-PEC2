using UnityEngine;

namespace Character.StateMachine
{
    public class FallingState : BaseState
    {
        public FallingState(string stateName, StateMachine ctx, StateFactory stateFactory) : base(stateName, ctx, stateFactory)
        {
        }

        public override void OnEnterState()
        {
            _ctx.CanShoot(true);
            _ctx.GetAnimationController.SetTrigger("IsFalling");
            _ctx.IsFalling = true;
            
            if (_ctx.GetRbVelocity().y > 0)
                _ctx.MultiplyYVelocityOnRb(0.5f);
            
            SetSubState(_stateFactory.HorizontalMoving);
        }

        protected override void Update()
        {
            CheckSwitchStates();
        }

        protected override void CheckSwitchStates()
        {
            if (_ctx.IsGrounded())
            {
                _ctx.ChangeState(_stateFactory.Grounded);
            }
        }

        public override void OnExitState()
        {
        }
    }
}