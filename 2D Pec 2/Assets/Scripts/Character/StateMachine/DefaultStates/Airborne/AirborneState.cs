namespace Character.StateMachine
{
    public class AirborneState :  BaseState
    {
        public AirborneState(string stateName, StateMachine ctx, StateFactory stateFactory) : base(stateName, ctx, stateFactory)
        {
            IsRootState = true;
        }

        public override void OnEnterState()
        {
            if (_ctx.IsJumping)
                SetSubState(_stateFactory.GoingUp);
            else
                SetSubState(_stateFactory.Falling);
            
            _ctx.CanShoot(true);
        }

        protected override void Update()
        {
        }

        protected override void CheckSwitchStates()
        {
        }

        public override void OnExitState()
        {
        }
    }
}