using UnityEngine;

namespace Character.StateMachine
{
    public abstract class BaseState
    {
        public bool IsRootState { get; protected set; } = false;
        protected StateMachine _ctx;
        protected StateFactory _stateFactory;
        protected string _stateName;

        private BaseState _superState;
        private BaseState _subState;
        
        protected BaseState(string stateName,StateMachine ctx, StateFactory stateFactory)
        {
            _stateName = stateName;
            _ctx = ctx;
            _stateFactory = stateFactory;
        }
        
        public abstract void OnEnterState();
        protected abstract void Update();
        protected abstract void CheckSwitchStates();
        public abstract void OnExitState();

        public void UpdateStates()
        {
            Update();
            if (_subState != null)
                _subState.UpdateStates();
        }
        
        public void SwitchThisState(BaseState newState)
        {
            if (_ctx.DoDebug)
                Debug.Log("Switching from " + _stateName + " to " + newState._stateName);
            
            OnExitState();
            if (IsRootState)
                _ctx.ChangeState(newState);
            else
            {
                _superState.SetSubState(newState);
            }
        }
        
        public void SetSuperState(BaseState superState)
        {
            _superState = superState;
        }
        
        public void SetSubState(BaseState subState)
        {
            if (_subState != null)
                _subState.OnExitState();
            
            _subState = subState;
            subState.SetSuperState(this);
            subState.OnEnterState();
        }
    }
}