namespace Character.StateMachine
{
    public class StateFactory
    {
        private StateMachine _ctx;
        // Root
        public BaseState Grounded => new GroundedState("default Grounded", _ctx, this);
        public BaseState Jump => new JumpState("default Jumping", _ctx, this);
        
        public BaseState Airborne => new AirborneState("default Airborne", _ctx, this);

        // SubStates
        
        public BaseState Idle => new IdleState("default Idle", _ctx, this);
        
        public BaseState HorizontalMoving =>  new HorizontalMovementState("default Horizontal", _ctx, this);
        
        public BaseState GoingUp => new GoingUpState("default GoingUp", _ctx, this);
        public BaseState Falling => new FallingState("default Falling", _ctx, this);
        public BaseState Landing => new LandingState("default Landing", _ctx, this);
        public BaseState GetBaseState()
        {
            return Grounded;
        }

        public StateFactory(StateMachine ctx)
        {
            _ctx = ctx;
        }
    }
}