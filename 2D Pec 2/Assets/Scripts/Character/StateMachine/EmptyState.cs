using UnityEngine;

namespace Character.StateMachine
{
    public class EmptyState : BaseState
    {
        public EmptyState(string stateName, StateMachine ctx, StateFactory stateFactory) : base(stateName, ctx, stateFactory)
        {
        }

        public override void OnEnterState()
        {
        }

        protected override void Update()
        {
            _ctx.DebugError(_stateName + " is empty");
        }

        protected override void CheckSwitchStates()
        {
        }

        public override void OnExitState()
        {
        }
        
    }
}