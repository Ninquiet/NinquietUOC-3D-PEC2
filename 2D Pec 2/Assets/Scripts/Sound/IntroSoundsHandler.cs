using UnityEngine;

namespace Sound
{
    public class IntroSoundsHandler : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip _buttonPressedClip;
        
        public void PlayButtonPressedSound()
        {
            _audioSource.PlayOneShot(_buttonPressedClip);
        }
    }
}