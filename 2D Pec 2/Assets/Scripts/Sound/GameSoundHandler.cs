using UnityEngine;

namespace Sound
{
    public enum AudioType
    {
        GameOver,
        PlayerHit,
        PlayerHeal,
        PlayerPickItem,
        PlayerShoot,
        PlayerShootExplosion,
        PlayerDie,
        PlayerJump,
        PlayerCheckpoint,
        PlayerWin,
        EnemyDie,
        EnemyHit,
        EnemyShoot
    }
    public class GameSoundHandler : MonoBehaviour
    {
        public static GameSoundHandler Instance;
        
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip _gameOverClip;
        [SerializeField] private AudioClip _playerHitClip;
        [SerializeField] private AudioClip _playerHealClip;
        [SerializeField] private AudioClip _playerPickItemClip;
        [SerializeField] private AudioClip _playerShootClip;
        [SerializeField] private AudioClip _playerShootExplosionClip;
        [SerializeField] private AudioClip _playerDieClip;
        [SerializeField] private AudioClip _playerJumpClip;
        [SerializeField] private AudioClip _playerCheckpointClip;
        [SerializeField] private AudioClip _playerWinClip;
        [SerializeField] private AudioClip _enemyDieClip;
        [SerializeField] private AudioClip _enemyHitClip;
        [SerializeField] private AudioClip _enemyShootClip;
        
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        
        public void PlayAudio(AudioType audioType)
        {
            switch (audioType)
            {
                case AudioType.GameOver:
                    _audioSource.PlayOneShot(_gameOverClip);
                    break;
                case AudioType.PlayerHit:
                    _audioSource.PlayOneShot(_playerHitClip);
                    break;
                case AudioType.PlayerHeal:
                    _audioSource.PlayOneShot(_playerHealClip);
                    break;
                case AudioType.PlayerPickItem:
                    _audioSource.PlayOneShot(_playerPickItemClip);
                    break;
                case AudioType.PlayerShoot:
                    _audioSource.PlayOneShot(_playerShootClip);
                    break;
                case AudioType.PlayerShootExplosion:
                    _audioSource.PlayOneShot(_playerShootExplosionClip);
                    break;
                case AudioType.PlayerDie:
                    _audioSource.PlayOneShot(_playerDieClip);
                    break;
                case AudioType.PlayerJump:
                    _audioSource.PlayOneShot(_playerJumpClip);
                    break;
                case AudioType.PlayerCheckpoint:
                    _audioSource.PlayOneShot(_playerCheckpointClip);
                    break;
                case AudioType.PlayerWin:
                    _audioSource.PlayOneShot(_playerWinClip);
                    break;
                case AudioType.EnemyDie:
                    _audioSource.PlayOneShot(_enemyDieClip);
                    break;
                case AudioType.EnemyHit:
                    _audioSource.PlayOneShot(_enemyHitClip);
                    break;
                case AudioType.EnemyShoot:
                    _audioSource.PlayOneShot(_enemyShootClip);
                    break;
            }
        }
    }
}