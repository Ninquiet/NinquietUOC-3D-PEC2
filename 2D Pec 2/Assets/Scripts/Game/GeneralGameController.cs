using Character.Life;
using Character.Player;
using Cinemachine;
using DG.Tweening;
using Save;
using Sound;
using UI;
using UnityEngine;

namespace Game
{
    public class GeneralGameController : MonoBehaviour
    {
        [SerializeField] private CinemachineVirtualCamera _virtualCamera;
        [SerializeField] private Transform _fakeFollowDot;
        [SerializeField] private Transform _playerHolder;
        [SerializeField] private Transform _player;
        [SerializeField] private Rigidbody2D _playerRigidbody2D;
        [SerializeField] private PlayerStateMachine _playerStateMachine;
        [SerializeField] private ShootingController _shootingController;
        [SerializeField] private Vector3 _playerDefaultPosition;
        [SerializeField] private LifeController _playerLifeController;
        [SerializeField] private UIController _uiController;
        private Vector3 _playerInitialPosition;
        private void Start()
        {
            Initialize();
            _playerLifeController.OnDeath += OnPlayerDeath;
        }

        [ContextMenu("ResetSaveData")]
        private void ResetSaveData()
        {
            SaveSystem.SaveCheckpoint(Vector3.zero);
        }

        public void OnVictory()
        {
            _playerLifeController.SetCanGetDamage(false);
            GameSoundHandler.Instance.PlayAudio(Sound.AudioType.PlayerWin);
            _playerHolder.position = _player.position + Vector3.up * 6;
            _playerHolder.gameObject.SetActive(true);
            _playerStateMachine.enabled = false;
            _shootingController.enabled = false;
            _shootingController.SetCanShoot(false);
            _playerRigidbody2D.isKinematic = true;
            _playerRigidbody2D.velocity = Vector2.zero;
            _playerHolder.DOMove(_player.position, 3f).OnComplete(() =>
            {
                _player.parent = _playerHolder;
                _player.localPosition = Vector3.zero;
                _fakeFollowDot.position = _player.position;
                _virtualCamera.Follow = _fakeFollowDot;
                var finalHolderPosition = _playerHolder.position+ Vector3.up * 10 + Vector3.left * 3 ;
                _playerHolder.DOMove(finalHolderPosition, 4).OnComplete(() =>
                {
                    _playerHolder.gameObject.SetActive(false);
                    _uiController.ShowVictoryScreen();
                });
            });
        }

        public void OnPlayerDeath()
        {
            _fakeFollowDot.position = _player.position;
            _virtualCamera.Follow = _fakeFollowDot;
            _playerStateMachine.enabled = false;
            _shootingController.enabled = false;
            _shootingController.SetCanShoot(false);
            
            Invoke(nameof(ShowDefeatScreen),1.3f);
        }

        private void ShowDefeatScreen()
        {
            _uiController.ShowDefeatScreen();
        }

        private void Initialize()
        {
            _playerStateMachine.enabled = false;
            _shootingController.enabled = false;
            _shootingController.SetCanShoot(false);
            _playerInitialPosition = SaveSystem.LoadCheckpoint();
            _playerRigidbody2D.isKinematic = true;
            DoPlayerEntrace();
        }

        private void DoPlayerEntrace()
        {
            var targetPosition = _playerInitialPosition == Vector3.zero ? _playerDefaultPosition : _playerInitialPosition;
            _fakeFollowDot.position = targetPosition;
            _virtualCamera.Follow = _fakeFollowDot;
            
            var playerHolderPosition = targetPosition;
            playerHolderPosition.y += 3;
            
            _playerHolder.position = playerHolderPosition;
            _playerHolder.DOMove(targetPosition, 3f).OnComplete(() =>
            {
                _player.parent = null;
                _virtualCamera.Follow = _player;
                _playerRigidbody2D.isKinematic = false;
                _playerStateMachine.enabled = true;
                _shootingController.enabled = true;
                _shootingController.SetCanShoot(true);
                
                var holderFinalPosition = _playerHolder.position + Vector3.up * 10;
                _playerHolder.DOLocalMove(holderFinalPosition, 4).OnComplete(() =>
                {
                    _playerHolder.gameObject.SetActive(false);
                });
            });
        }
    }
}