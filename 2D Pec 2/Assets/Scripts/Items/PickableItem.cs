using System;
using Character.Life;
using Character.Player;
using DG.Tweening;
using Sound;
using UnityEngine;
using AudioType = Sound.AudioType;

namespace Items
{
    public enum KindOfItem
    {
        Munition,
        Health,
    }

    public class PickableItem : MonoBehaviour
    {
        [SerializeField] private KindOfItem _kindOfItem;
        [SerializeField] private SpriteRenderer _borderSpriteRenderer;

        private bool _colorIsWhite;
        
        private void Start()
        {
            ChangeBorderColor();
        }

        private void ChangeBorderColor()
        {
           _borderSpriteRenderer.DOColor( _colorIsWhite ? Color.white : Color.magenta, 0.3f).SetLoops(-1, LoopType.Yoyo);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                if (_kindOfItem == KindOfItem.Munition)
                {
                    if (other.TryGetComponent(out ItemInventory itemInventory))
                    {
                        itemInventory.AddMunitions(5);
                        GameSoundHandler.Instance.PlayAudio(AudioType.PlayerPickItem);
                        Destroy(gameObject);
                    }
                }
                if (_kindOfItem == KindOfItem.Health)
                {
                    if (other.TryGetComponent(out LifeController lifeController))
                    {
                        lifeController.Heal(1);
                        GameSoundHandler.Instance.PlayAudio(AudioType.PlayerPickItem);
                        Destroy(gameObject);
                    }
                }
            }
        }

        private void OnDestroy()
        {
            _borderSpriteRenderer.DOKill();
        }
    }
}