using Character.Life;
using Sound;
using UnityEngine;

namespace Bullets
{
    public class BasicBullet : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private int _damage;
        [SerializeField] private Rigidbody2D _rigidbody2D;
        [SerializeField] private LayerMask _environmentLayer;
        [SerializeField] private GameObject _explosionPrefab;
        [SerializeField] private GameObject _EnviromentHitPrefab;
        
        private Vector2 _direction;
        private float duration = 3.5f;
        private LayerMask _enemyLayer;
        private bool _initialized = false;
        private float _timeSinceInitialized; 
        private Vector3 _hitPosition;
        
        public void Initialize (Vector2 targetPosition, LayerMask enemyLayer, float multiplier = 1f)
        {
            SetTargetDirection(targetPosition);
            _rigidbody2D.velocity = _direction * _speed * multiplier;
            _enemyLayer = enemyLayer;
            transform.localRotation = Quaternion.Euler(0, 0, Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg);
            Destroy(gameObject, duration);
            _timeSinceInitialized = Time.time;
            _initialized = true;
        }
        
        private void Update()
        {
            if (!_initialized) return;
            transform.localRotation = Quaternion.Euler(0, 0, Mathf.Atan2(_rigidbody2D.velocity.y, _rigidbody2D.velocity.x) * Mathf.Rad2Deg);
        }
        
        private void SetTargetDirection(Vector2 targetPosition)
        {
            _direction = (targetPosition - (Vector2)transform.position).normalized;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            _hitPosition = other.contacts[0].point;
            var isOnEnemyLayer = ((1 << other.gameObject.layer) & _enemyLayer) != 0;
            var isOnEnvironmentLayer = ((1 << other.gameObject.layer) & _environmentLayer) != 0;
            if (isOnEnemyLayer)
            {
                other.gameObject.GetComponent<LifeController>()?.TakeDamage(_damage);
                SpawnExplosion();
                Destroy(gameObject);
            }
            else if (isOnEnvironmentLayer)
            {
                if (Time.time - _timeSinceInitialized < 0.1f) 
                    return;

                SpawnEnvironmentHit();
                GameSoundHandler.Instance.PlayAudio(Sound.AudioType.PlayerShootExplosion);
                Destroy(gameObject);
            }
        }
        
        private void SpawnExplosion()
        {
            Instantiate(_explosionPrefab, _hitPosition, Quaternion.identity);
        }
        
        private void SpawnEnvironmentHit()
        {
            Instantiate(_EnviromentHitPrefab, _hitPosition, Quaternion.identity);
        }
    }
}