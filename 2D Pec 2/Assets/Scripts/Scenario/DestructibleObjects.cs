using System;
using Character.Life;
using UnityEngine;

namespace Scenario
{
    public class DestructibleObjects : MonoBehaviour
    {
        [SerializeField] private LifeController _objectLifeController;
        [SerializeField] private GameObject _destoyedParticlePrefab;
        [SerializeField] private GameObject _objectToSpawn;

        private void Awake()
        {
            _objectLifeController.OnDeath += DestroyObject;
        }

        private void DestroyObject()
        {
            if (_destoyedParticlePrefab != null)
                Instantiate(_destoyedParticlePrefab, transform.position, Quaternion.identity);
            if (_objectToSpawn != null)
                Instantiate(_objectToSpawn, transform.position, Quaternion.identity);
            
            Destroy(gameObject);
        }
    }
}