using System;
using DG.Tweening;
using UnityEngine;

namespace UI
{
    public class IntroAnimation : MonoBehaviour
    {
        [SerializeField] private Transform _alienTransform;
        [SerializeField] private Transform _humanTransform;
        
        private Camera _mainCamera;
        private void Start()
        {
            _mainCamera = Camera.main;
            DoEffect();
        }

        private void DoEffect()
        {
            var cameraHorizontalSize = _mainCamera.orthographicSize * _mainCamera.aspect;
            
            var alienInitialPosition = _alienTransform.position;
            var alienY = alienInitialPosition.y;
            alienInitialPosition = Vector3.left * cameraHorizontalSize * 1.3f;
            alienInitialPosition.y = alienY;
            _alienTransform.position = alienInitialPosition;
            
            var humanInitialPosition = _humanTransform.position;
            var humanY = humanInitialPosition.y;
            humanInitialPosition = Vector3.left * cameraHorizontalSize * 1f;
            humanInitialPosition.y = humanY;
            _humanTransform.position = humanInitialPosition;
            
            var alienTargetPosition = alienInitialPosition + Vector3.right * cameraHorizontalSize * 2;
            var humanTargetPosition = humanInitialPosition + Vector3.right * cameraHorizontalSize * 2;
            _alienTransform.DOMove(alienTargetPosition, 3);
            _humanTransform.DOMove(humanTargetPosition, 3).OnComplete(() =>
            {
                //DoEffect();
            });
        }
    }
}