using System;
using DG.Tweening;
using Save;
using UnityEngine;

namespace UI
{
    public class  IntroUI : MonoBehaviour
    {
        [SerializeField] private CanvasGroup UI1;
        [SerializeField] private CanvasGroup UI2;

        private void Start()
        {
            EnableCanvasGroup(UI1, true);
            EnableCanvasGroup(UI2, false);
        }

        public void PlayGame()
        {
            EnableCanvasGroup(UI1, false);
            EnableCanvasGroup(UI2, true);
        }
        
        public void SelectSaveSlot(int slot)
        {
            SaveSystem.SetCurrentSlot(slot);
            UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        }
        
        public void EraseSaveSlot(int slot)
        {
            SaveSystem.SetCurrentSlot(slot);
            SaveSystem.SaveCheckpoint(Vector3.zero);
        }

        private void EnableCanvasGroup(CanvasGroup canvasGroup, bool action)
        {
            canvasGroup.interactable = action;
            canvasGroup.blocksRaycasts = action;
            canvasGroup.DOFade( action ? 1 : 0, 0.5f);
        }
        
    }
}