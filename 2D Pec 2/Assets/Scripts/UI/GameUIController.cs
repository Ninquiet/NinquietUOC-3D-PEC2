using System;
using Character.Player;
using TMPro;
using UnityEngine;

namespace UI
{
    public class GameUIController : MonoBehaviour
    {
        [SerializeField] private ItemInventory _itemInventory;
        [SerializeField] private TMP_Text _munitionsText;

        private void Start()
        {
            _itemInventory.OnMunitionsChanged += UpdateMunitionsText;
            UpdateMunitionsText();
        }

        private void UpdateMunitionsText()
        {
            _munitionsText.text = _itemInventory.GetMunitions().ToString();
        }
    }
}