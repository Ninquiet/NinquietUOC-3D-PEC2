using System;
using Character.Life;
using Save;
using Sound;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class UIController : MonoBehaviour
    {
        [SerializeField] GameObject _vicotryScreen;
        [SerializeField] GameObject _defeatScreen;
        [SerializeField] GameObject _pauseScreen;

        private bool _isGameRunning = true;

        private void Start()
        {
            _vicotryScreen.SetActive(false);
            _defeatScreen.SetActive(false);
            _pauseScreen.SetActive(false);
        }

        public void  ShowVictoryScreen()
        {
            _vicotryScreen.SetActive(true);
            _isGameRunning = false;
        }

        public void ShowDefeatScreen()
        {
            _defeatScreen.SetActive(true);
            _isGameRunning = false;
        }
        
        public void PlayAgain()
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }

        public void ReturnToMainMenu()
        {
            SceneManager.LoadScene(0);
        }
        
        public void Quit()
        {
            Application.Quit();
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!_isGameRunning)
                    return;
                PauseScreen(!_pauseScreen.activeSelf);
            }
        }
        
        private void PauseScreen (bool value)
        {
            _pauseScreen.SetActive(value);
        }
    }
}